<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@start');
Route::any('/game', 'HomeController@game'); // Randomize Image

Auth::routes();

Route::get('/home', 'HomeController@game');
// Route::get('/random', 'GameContoller@randomImage');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');

    Route::get('/change-password', 'Auth\UpdatePasswordController@view');
    Route::post('/change-password', 'Auth\UpdatePasswordController@update');

    /**
     * Menu
     */
    Route::group(['prefix' => 'menu'],function() {
        Route::any('/', 'MenuController@listMenu');
        Route::get('create', 'MenuController@create');
        Route::get('update/{plu_id}', 'MenuController@selected');
        
        Route::post('create', 'MenuController@createPost');
        Route::post('update/{plu_id}', 'MenuController@update');
        Route::post('delete', 'MenuController@delete');
    });

    /**
     * Voucher
     */
    Route::group(['prefix' => 'voucher'],function() {
        Route::any('/', 'VoucherController@listMenu');
        Route::any('create', 'VoucherController@create');
        Route::post('delete', 'VoucherController@delete');
        Route::post('delete/all/free', 'VoucherController@deleteAllVoucherFree');
        Route::any('{type}', 'VoucherController@listVoucherBy');
    });

    /**
     * Report
     */
    Route::group(['prefix' => 'report'],function() {
        Route::any('/', 'ReportController@report');
        Route::any('by-date', 'ReportController@reportByDate');
        Route::any('winner', 'ReportController@reportWinner');
    });

    /**
     * Setting
     */
    Route::group(['prefix' => 'setting'],function() {
        Route::any('/', 'SettingController@getSetting');
    });
});


