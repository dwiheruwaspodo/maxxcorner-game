<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Maxx Corner | Tebak Harga</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- <link rel="stylesheet" href="{{ url('css/prefixfree.css') }}"> -->
		<link rel="stylesheet" href="{{ url('css/style.css') }}">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="page-bg">
			<div class="container">
				<div class="logo-area">
					<center>
						<img src="{{ url('assets/layouts/layout/img/logo.png') }}" alt="Logo Maxx Corner" class="image-logo">
					</center>
				</div>
				<div class="game-container">
					<div class="row pad-row">
						<div class="col-md-3"></div>

						<div class="col-md-3">
							<div class="boxed" data-location="0">
								<img src="http://placehold.it/500x500" alt="" class="img-responsive cover">
								<ul class="flip" id="flip_0">
									<li>
										<img src="http://placehold.it/500x500&text=gambar1" alt="" class="img-responsive">
										<center>Test 1</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar2" alt="" class="img-responsive">
										<center>Test 2</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar3" alt="" class="img-responsive">
										<center>Test 3</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar4" alt="" class="img-responsive">
										<center>Test 4</center>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="boxed" data-location="1">
								<img src="http://placehold.it/500x500" alt="" class="img-responsive cover">
								<ul class="flip" id="flip_1">
									<li>
										<img src="http://placehold.it/500x500&text=gambar1" alt="" class="img-responsive">
										<center>Test 1</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar2" alt="" class="img-responsive">
										<center>Test 2</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar3" alt="" class="img-responsive">
										<center>Test 3</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar4" alt="" class="img-responsive">
										<center>Test 4</center>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3"></div>
					</div>
					<div class="row pad-row">
						<div class="col-md-3"></div>

						<div class="col-md-3">
							<div class="boxed" data-location="2">
								<img src="http://placehold.it/500x500" alt="" class="img-responsive cover">
								<ul class="flip" id="flip_2">
									<li>
										<img src="http://placehold.it/500x500&text=gambar1" alt="" class="img-responsive">
										<center>Test 1</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar2" alt="" class="img-responsive">
										<center>Test 2</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar3" alt="" class="img-responsive">
										<center>Test 3</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar4" alt="" class="img-responsive">
										<center>Test 4</center>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="boxed" data-location="3">
								<img src="http://placehold.it/500x500" alt="" class="img-responsive cover">
								<ul class="flip" id="flip_3">
									<li>
										<img src="http://placehold.it/500x500&text=gambar1" alt="" class="img-responsive">
										<center>Test 1</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar2" alt="" class="img-responsive">
										<center>Test 2</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar3" alt="" class="img-responsive">
										<center>Test 3</center>
									</li>
									<li>
										<img src="http://placehold.it/500x500&text=gambar4" alt="" class="img-responsive">
										<center>Test 4</center>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3"></div>
					</div>
				</div>
				<button class="start btn">Start</button>
			</div>
		</div>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="{{ url('js/jquery.animate-enhanced.min.js') }}"></script>
		<script>
			$(document).find('li').hide();
			$(document).ready(function() {
				var delay = 400;

				function randomize( id ) {
					var random = Math.floor(Math.random() * $(id).find('li').length);
					$(id).find('li').hide();
					$( id + ' > li:eq(' + random + ')').fadeIn();
				}
				
				$('.cover').hide();

				var funct_random_0 = setInterval(randomize, delay, '#flip_0');
				var funct_random_1 = setInterval(randomize, delay, '#flip_1');
				var funct_random_2 = setInterval(randomize, delay, '#flip_2');
				var funct_random_3 = setInterval(randomize, delay, '#flip_3');

				$.ajax({
					url: 'http://tebak.maxx',
					data: '',
					method: 'get',
					success: function(data) {
						setTimeout(function() {
							clearInterval(funct_random_0);
							clearInterval(funct_random_1);
							clearInterval(funct_random_2);
							clearInterval(funct_random_3);


						}, 5000);	
					},
					error: function(data) {
						console.log('test');
					}
				});
							
			});
		</script>
	</body>
</html>