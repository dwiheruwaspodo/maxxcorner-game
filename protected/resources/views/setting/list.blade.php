@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ url('admin') }}">
        Home
      </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Setting</span>
    </li>
  </ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->

@include('notifications')

<div class="portlet light bordered">
  <form action="{{ url('admin/setting') }}" method="post">
  {{ csrf_field() }}
  <div class="portlet-body">
    <div class="tabbalble-bordered">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#time" data-toggle="tab">Time & Expired </a></li>
        
        @for ($x=1; $x <= 4; $x++)
        <li><a href="#voucher{{ $x }}" data-toggle="tab">Range Voucher {{ $x }}</a></li>
        @endfor
      </ul>
    </div>
    <div class="tab-content">
     
        <div id="time" class="tab-pane active">
          <div class="form-body form-horizontal">
            <div class="form-group">
                <label class="col-md-2 control-label"> Timer  <span class="required" aria-required="true"> * </span>
                <br>
                <span class="required" aria-required="true"> (in second) </span>
                </label>
                <div class="col-md-5">
                    <div class="input-icon right">
                      @if (isset($time))
                        <input class="form-control" type="text" name="time" value="{{ $time }}" required>
                      @else
                        <input class="form-control" type="text" name="time" value="{{old('time')}}" required>
                      @endif
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label"> Voucher Expired  <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-5">
                  <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                    @if (isset($voucher_expired))
                      <input type="text" name="voucher_expired" class="form-control" value="{{ date('Y-m-d', strtotime($voucher_expired)) }}" 
                      required>
                    @else
                      <input type="text" name="voucher_expired" class="form-control" value="{{old('time')}}" 
                      required>
                    @endif
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                              <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                  </div>  
                </div>
            </div>

          </div>
        </div>
        
        @for ($x=1; $x <= 4; $x++)
        <div id="voucher{{ $x }}" class="tab-pane">
          <div class="form-body form-horizontal">
            <div class="form-group">
                <label class="col-md-2 control-label"> Top Range<span class="required" aria-required="true"> * </span></label>
                <div class="col-md-5">
                    <div class="input-icon right">
                      <?php $max = "max_".$x; ?>
                      @if (isset($$max))
                        <input class="form-control" type="text" name="max_{{ $x }}" value="{{ $$max }}" required>
                      @else 
                        <input class="form-control" type="text" name="max_{{ $x }}" value="{{old('max_'.$x)}}" required>
                      @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label"> Bellow Range <span class="required" aria-required="true"> * </span></label>
                <div class="col-md-5">
                    <div class="input-icon right">
                    <?php $min = "min_".$x; ?>
                      @if (isset($$min))
                        <input class="form-control" type="text" name="min_{{ $x }}" value="{{ $$min }}" required>
                      @else
                        <input class="form-control" type="text" name="min_{{ $x }}" value="{{old('min')}}" required>
                      @endif
                    </div>
                </div>
            </div>
          </div>
        </div>
        @endfor
      
    </div>
  </div>
  
  <hr>
  <center><button type="submit" class="btn btn md green">Update All</button></center>
   
  </form>
</div>



@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>

@endsection

@section('pagejs3')

@endsection