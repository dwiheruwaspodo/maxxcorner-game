@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> 
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Report</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->

<div class="row">
  <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
      <div class="display">
        <div class="number">
          <h3 class="font-red-haze">
            <span data-counter="counterup" data-value="{{$money_out}}">Rp{{ number_format($money_out, 2) }}</span>
          </h3>
          <small>TOTAL SPENDING</small>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
      <div class="display">
        <div class="number">
          <h3 class="font-blue-sharp">
            <span data-counter="counterup" data-value="{{ $menu_out }}">{{ $menu_out }}</span>
          </h3>
          <small>TOTAl MENU OUT</small>
        </div>
        <div class="icon">
          <i class="fa fa-cutlery"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            <span data-counter="counterup" data-value="{{ $total_menu }}">{{ $total_menu }}</span>
          </h3>
          <small>VARIANT MENU</small>
        </div>
        <div class="icon">
          <i class="icon-action-redo"></i>
        </div>
      </div>
    </div>
  </div> 
</div> 

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			
		</div>
	</div>
	<div class="portlet-body">
        <table  class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
          <thead>
            <tr>
              <th class="all">PLU ID</th>
              <th class="all">Name</th>
              <th class="all">Prices</th>
              <th class="all">Picture</th>
              <th class="all">Out</th>
              <th class="all">Total Prices</th>
            </tr>
          </thead>
          <tbody>
          <?php setlocale(LC_MONETARY, 'id_ID'); ?>
          @if(!empty($menu_list))
            @foreach($menu_list as $key=>$row)
            <tr>
              <td> {{ $row['plu_id'] }} </td>
              <td> {{ $row['name'] }} </td>
              <td> Rp{{ number_format($row['prices'], 2) }} </td>
              <td> <center> <img src="{{ $row['url_picture'] }}" width="100px" height="100px" alt="{{ $row['name'] }}"> </center> </td>
              <td> {{ $row['total_out'] }} </td>
              @if ($row['total_prices'] == 0)
              <td> - </td>
              @else
              <td> Rp{{ number_format($row['total_prices'], 2) }} </td>
              @endif
            </tr>
            @endforeach
          @endif
          </tbody>
        </table>
	</div>
</div>

@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

<script src="{{ url('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
  	var token = "<?php echo csrf_token();?>";
    $('#sample_1').dataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ entries",
            infoEmpty: "No entries found",
            infoFiltered: "(filtered1 from _MAX_ total entries)",
            lengthMenu: "_MENU_ entries",
            search: "Search:",
            zeroRecords: "No matching records found"
        },
        buttons: [{
            extend: "print",
            className: "btn dark btn-outline",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
          extend: "copy",
          className: "btn blue btn-outline",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        },{
          extend: "pdf",
          className: "btn yellow-gold btn-outline",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }, {
            extend: "excel",
            className: "btn green btn-outline",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
            extend: "csv",
            className: "btn purple btn-outline ",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
          extend: "colvis",
          className: "btn red",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }],
        columnDefs: [{
            className: "control",
            orderable: !1,
            targets: 0
        }],
        order: [0, "asc"],
        lengthMenu: [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],
        pageLength: 10,
        "bPaginate" : true,
        "bInfo" : true,
        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
    }); 

  

  });
</script>

@endsection

@section('pagejs3')

@endsection