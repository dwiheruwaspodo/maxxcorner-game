@extends('layouts.game_start')

@section('pagebody')
<div id="space"></div>
<center>
	<div class="table-container">
		<div class="table-content">
		
			@if(count($menu) > 0)
			<form action="{{ url('game') }}" method="post">
				{{ csrf_field() }}
				<button class="btn btn-bg btn-default btn-maxx" autofocus>Start</button>
			</form>
			@else 
			
			<h1 class="color-white">Please set your menu first!</h1>

			@endif
		</div>
	</div>
</center>
	
@endsection

@section('pagejs')

@endsection