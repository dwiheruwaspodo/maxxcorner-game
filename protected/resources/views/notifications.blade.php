@if($errors->any())
  @if($errors->all()[0] != 'empty')
  <div class="alert alert-danger">
   <strong>Error!</strong> <br/>

     @foreach($errors->all() as $e)
       - <?php echo $e ?> <br/>
     @endforeach
  </div>
 @endif
@endif

@if(Session::has('exception'))
  <div class="alert alert-danger">
      <?php $e = Session::get('exception'); ?>
   <strong>Error ({{$e['exception']}}</strong> <br/>
   <i>{{$e['error']}}</i><br/>
   <span>{{$e['message']}}</span>
  </div>
  <?php Session::forget('exception'); ?>
@endif

@if(session()->exists('success'))
  <div class="alert alert-success">
   <strong>Success!</strong><br/>

   @foreach(session('success') as $s)
     - {!! $s !!} <br/>
   @endforeach
 </div>
 <?php Session::forget('success'); ?>
@endif

@if(Session::has('warning'))
  <div class="alert alert-warning">
    <strong>Warning!</strong> <br/>
    @foreach(Session::get('warning') as $w)
        - <?php echo $w ?> <br/>
    @endforeach

 </div>
 <?php Session::forget('warning'); ?>
@endif
