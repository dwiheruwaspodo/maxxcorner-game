<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Maxx Corner | Tebak Harga</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{ url('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
		<!-- <link rel="stylesheet" href="{{ url('css/prefixfree.css') }}"> -->
		@yield('pagecss')
		<link rel="stylesheet" href="{{ url('css/style.css') }}">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="page-bg-start">
		<div>
			<div class="container">
				@yield('pagebody')
			</div>
		</div>
		@yield('additionalbody')
		<!-- jQuery -->
		<script src="{{ url('assets/global/plugins/jquery.min.js') }}"></script>
		<script>
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		</script>
		<!-- Bootstrap JavaScript -->
		<script src="{{ url('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
		@yield('pagejs')
	</body>
</html>