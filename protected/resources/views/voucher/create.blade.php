@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Voucher</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->

@include('notifications')

<div class="portlet light bordered">
    <form id="form" class="form-horizontal" action="{{ url('admin/voucher/create') }}" method="post" enctype="multipart/form-data">
    <div class="portlet-title">
      <div class="form-group">
        <div class="col-md-12" align="center">
          <div class="md-radio-inline">
            <div class="md-radio">
              <input name="type" class="md-radiobtn chooseType" type="radio" id="random" value="random" required>
              <label for="random">
                <span></span>
                <span class="check"></span>
                <span class="box"></span> Random </label>
            </div>
            <div class="md-radio">
              <input name="type" class="md-radiobtn chooseType" type="radio" id="manual" value="manual" required>
              <label for="manual">
                <span></span>
                <span class="check"></span>
                <span class="box"></span> Create Manual </label>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="portlet-body">
        {{csrf_field()}}
        <div class="form-body">
          <div class="form-group" id="catcat" style="display: none;">
            <label class="col-md-2 control-label">Category</label>
            <div class="col-md-5">
              <select autocomplete="off" data-type="products" name="category" class="form-control select2-multiple select2-hidden-accessible from_product"  tabindex="-1" aria-hidden="true" placeholder="Select Category">
              <option value="">Select Category</option>
                  @for ($x=1; $x <= 4; $x++ )
                    <option value="{{ $x }}"> Voucher for {{ $x }} Menu </option>
                  @endfor
              </select>
            </div>
          </div>

          <div class="form-group pilihan" id="create_manual" style="display: none;">
              <label class="col-md-2 control-label">Code <span class="required" aria-required="true"> * </span>
                  <br>
                  <span class="required" aria-required="true"> Separated by new line </span>
              </label>
              <div class="col-md-5">
                  <div class="input-icon right">
                      <textarea class="form-control inp manual" rows="10" name="code" required>{{old('code')}}</textarea>
                  </div>
              </div>
          </div>

          <div class="form-group pilihan" id="create_random" style="display: none;">
              <label class="col-md-2 control-label">Total <span class="required" aria-required="true"> * </span></label>
              <div class="col-md-5">
                  <div class="input-icon right">
                      <input class="form-control inp random" type="text" name="jumlah" value="{{old('jumlah')}}" required>
                  </div>
              </div>
          </div>

          <br>
          <div class="form-group submit" style="display: none;">
            <label class="col-md-2 control-label"> </label>
            <div class="col-md-10">
              <button type="submit" class="btn btn md green">Create</button>
            </div>
          </div>
        </div>
    </div>
    </form>
  </div>

@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script>
  $(document).ready(function(){
    $('.chooseType').click(function(){
      $('#catcat').show();
      
      var type = $(this).val();

      $('.pilihan').hide();

      $('#create_'+type).show();

      $('.inp').removeAttr('required');
      $('.'+type).prop('required', true);

      $('.submit').show();
    });
  });

</script>

@endsection

@section('pagejs3')

@endsection