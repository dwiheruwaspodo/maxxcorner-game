@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> 
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Voucher</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->
@if ($type == "used")
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption">
      
      <span class="caption-subject font-green bold uppercase">Search</span>
    </div>
  </div>
    <div class="portlet-body">

      <form id="form" class="form-horizontal" action="{{ url('admin/voucher/used') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-body">
          <div class="form-group">
              <label class="col-md-2 control-label">Date Range <span class="required" aria-required="true"> * </span></label>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-4">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="start_date" class="form-control" required value="{{ $start_date }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                  </div>
                  <label class="col-md-2 control-label">End <span class="required" aria-required="true"> * </span></label>
                  <div class="col-md-4">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="end_date" class="form-control" required value="{{ $end_date }}">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>  
                  </div>
                </div>
              </div>
          </div>
          <br>
          <div class="form-group">
            <label class="col-md-2 control-label"> </label>
            <div class="col-md-10">
              <button type="submit" class="btn btn md green">Search <i class="fa fa-search"></i></button>
            </div>
          </div>

        </div>
      </form>

    </div>
</div>
@endif

<?php setlocale(LC_MONETARY, 'id_ID'); ?>

<div class="row">
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
          <div class="visual">
              <i class="fa fa-comments"></i>
          </div>
          <div class="details">
              <div class="number">
                  <span data-counter="counterup" data-value="{{ $v_1 }}">{{ $v_1 }}</span>
              </div>
              <div class="desc"> Voucher 1 Menu </div>
          </div>
      </a>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a class="dashboard-stat dashboard-stat-v2 red" href="#">
          <div class="visual">
              <i class="fa fa-bar-chart-o"></i>
          </div>
          <div class="details">
              <div class="number">
                  <span data-counter="counterup" data-value="{{ $v_2 }}">{{ $v_2 }}</span>
              </div>
              <div class="desc"> Voucher 2 Menu </div>
          </div>
      </a>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a class="dashboard-stat dashboard-stat-v2 green" href="#">
          <div class="visual">
              <i class="fa fa-shopping-cart"></i>
          </div>
          <div class="details">
              <div class="number">
                  <span data-counter="counterup" data-value="{{ $v_3 }}">{{ $v_3 }}</span>
              </div>
              <div class="desc"> Voucher 3 Menu </div>
          </div>
      </a>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
          <div class="visual">
              <i class="fa fa-globe"></i>
          </div>
          <div class="details">
              <div class="number"> 
                  <span data-counter="counterup" data-value="{{ $v_4 }}">{{ $v_4 }}</span>
              </div>
              <div class="desc"> Voucher 4 Menu </div>
          </div>
      </a>
  </div> 
</div> 

<div class="row">
  <!-- <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
      <div class="display">
        <div class="number">
          <h3 class="font-red-haze">
            <span data-counter="counterup" data-value="{{ $v_1 + $v_2 + $v_3 + $v_4 }}">{{ $v_1 + $v_2 + $v_3 + $v_4 }}</span>
          </h3>
          <small>TOTAL WINNER</small>
        </div>
        <div class="icon">
          <i class="fa fa-trophy"></i>
        </div>
      </div>
    </div>
  </div> -->
  <div class="col-md-offset-6 col-md-6">
    <div class="dashboard-stat2 bordered">
      <div class="display">
        <div class="number">
          <h3 class="font-blue-sharp">
            <span data-counter="counterup" data-value="{{ $v_1 + $v_2 + $v_3 + $v_4 }}">{{ $v_1 + $v_2 + $v_3 + $v_4 }}</span>
          </h3>
          <small>TOTAL VOUCHER</small>
        </div>
        <div class="icon">
          <i class="fa fa-credit-card"></i>
        </div>
      </div>
    </div>
  </div>
</div> 

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
      @if ($type != "used")
			<a type="button" id="deleteAllFree" class="btn red">Delete All Free Voucher</a>
      @endif
		</div>
	</div>
	<div class="portlet-body">
        <table  class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
          <thead>
            <tr>
              @if ($type == "used")
              <th class="all">Date</th>
              @endif
              <th class="all">Code</th>
              <th class="all">Category</th>
              @if ($type != "used")
              <th class="all">Actions</th>
              @else
              <th class="all">Menu</th>
              @endif
            </tr>
          </thead>
          <tbody>
          
          @if(!empty($data))
            @foreach($data as $key=>$row)
            <tr>
              @if ($type == "used")
              <td> <b> {{ date('Y-m-d', strtotime($row->updated_at)) }} </b> <small> ( {{ date('l', strtotime($row->updated_at)) }} on {{ date('F Y', strtotime($row->updated_at)) }} ) </small></td>
              @endif
              <td> {{ $row->code }} </td>
              <td> For {{ $row->category }} Menu</td>
              @if ($type != "used")
              <td class="noExport">
                  <a class="delete btn green btn-xs btn-outline" data-id="{{ $row->id }}"> <i class="fa fa-trash"></i> Delete
                  </a>
              </td>
              @else
                <td>
                  <?php
                    $menu = json_decode(json_encode($row->menu), true);
                    $menu = array_pluck($menu, 'name');
                    $menu = implode(", ", $menu);

                    echo $menu;
                  ?>
                </td>
              @endif
            </tr>
            @endforeach
          @endif
          </tbody>
        </table>
        {{ $data->links() }}
	</div>
</div>



@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

<script src="{{ url('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>

<script src="{{ url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
  	var token = "<?php echo csrf_token();?>";
    $('#sample_1').dataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ entries",
            infoEmpty: "No entries found",
            infoFiltered: "(filtered1 from _MAX_ total entries)",
            lengthMenu: "_MENU_ entries",
            search: "Search:",
            zeroRecords: "No matching records found"
        },
        buttons: [{
            extend: "print",
            className: "btn dark btn-outline",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
          extend: "copy",
          className: "btn blue btn-outline",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }, {
          extend: "colvis",
          className: "btn red",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }],
        columnDefs: [{
            className: "control",
            orderable: !1,
            targets: 0
        }],
        order: [0, "asc"],
        lengthMenu: [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],
        pageLength: 10,
        bFilter: false,
        bPaginate : false,
        bInfo : true,
        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
    }); 

    $('#sample_1').on('click', '.delete', function(){      
      var id     = $(this).data('id');
      var column = $(this).parents('tr');

    	swal({
    	  title: "Are you sure?",
    	  type: "warning",
    	  showCancelButton: true,
    	  confirmButtonColor: "#DD6B55",
    	  confirmButtonText: "Yes, delete it!",
    	  closeOnConfirm: false
    	},
    	function(){
        $.ajax({
            type    : "POST",
            url     : "<?php echo url('admin/voucher/delete')?>",
            data    : "_token="+token+"&id="+id,
            success : function(result) {
              if (result === "yes") {
                swal({
                    title: "Vouhcer has been deleted",
                    timer: 1500,
                    type: "success",
                    showConfirmButton: false
                });

                $('#sample_1').DataTable().row(column).remove().draw();
              }
              else {
    	          swal("Deleted!", "Vouhcer unsuccessfully deleted.", "error");
              }
            }
        });
    	});
    });

  });
</script>
<script>
  $("#deleteAllFree").click(function() {
    var token = "<?php csrf_token(); ?>";
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this voucher!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            'type' : 'POST',
            'url'  : '<?php echo url("admin/voucher/delete/all/free") ?>',
            'data' : '_token='+token,
            success : function(result) {
              if (result == "yes") {
                swal("Deleted!", "Voucher has been deleted.", "success");
                
                setTimeout(function(){ 
                  location.reload();
                }, 3000);
              }
              else {
                swal("Error", "Voucher fail to deleted.", "error");
                setTimeout(function(){ 
                  location.reload();
                }, 3000);
              }
            }
          });
        }
    });
  });

</script>

@endsection

@section('pagejs3')

@endsection