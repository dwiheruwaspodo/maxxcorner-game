@extends('layouts.app')

@section('pagecss')

@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ url('admin') }}">
        Home
      </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Change Password</span>
    </li>
  </ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">Change Password</h1>
<!-- END PAGE TITLE -->

@if (Session::has('success'))
    <div class="alert alert-success">
    	{!! Session::get('success') !!}
    </div>
@endif
@if (Session::has('failure'))
    <div class="alert alert-danger">
    	{!! Session::get('failure') !!}
    </div>
@endif

	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">

			</div>
		</div>
		<div class="portlet-body">
			<form id="form" class="form-horizontal" action="{{ url('admin/change-password') }}" method="post" >
				{{ csrf_field() }}
				<div class="form-body">
					<div class="form-group">
						<label for="old" class="col-md-3 control-label">Old Password*</label>
						<div class="col-md-6">
							<input type="password" name="old" class="form-control" placeholder="Old Password" id="old" autofocus required>
							@if ($errors->has('old'))
                                <span class="help-block font-red">
                                    <strong>{{ $errors->first('old') }}</strong>
                                </span>
                            @endif
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 control-label">New Password*</label>
						<div class="col-md-6">
							<input type="password" name="password" class="form-control" placeholder="New Password" id="password" required>
							@if ($errors->has('password'))
                                <span class="help-block font-red">
                                	<strong>{{ $errors->first('password') }}</strong>
                            	</span>
                            @endif
						</div>
					</div>
					<div class="form-group">
						<label for="rpassword" class="col-md-3 control-label">Retype New Password*</label>
						<div class="col-md-6">
							<input type="password" name="password_confirmation" class="form-control" placeholder="Retype New Password" id="rpassword" required>
							@if ($errors->has('password_confirmation'))
                                <span class="help-block font-red">
                                	<strong>{{ $errors->first('password_confirmation') }}</strong>
                            	</span>
                            @endif
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success pull-right">Save</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>

@endsection

@section('pagejs1')

@endsection

@section('pagejs2')
@endsection

@section('pagejs3')

@endsection