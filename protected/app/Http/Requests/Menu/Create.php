<?php

namespace App\Http\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class Create extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plu_id'  => 'required|unique:menu,plu_id',
            'name'    => 'required',
            'prices'  => 'required|integer',
            'picture' => 'required|dimensions:min_width=300,min_height=300,max_width=5000,max_height=5000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    protected function validationData()
    {
        return $this->all();
    }
}

