<?php

namespace App\Http\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'id'      => 'required|integer',
            'plu_id'  => 'required',
            'name'    => 'required',
            'prices'  => 'required',
            'picture' => 'sometimes|required|dimensions:min_width=300,min_height=300,max_width=5000,max_height=5000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    protected function validationData()
    {
        return $this->all();
    }
}
