<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Voucher;
use App\Win;
use DB;

use App\Lib\MyHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class VoucherController extends Controller
{
    public $saveImage;
    public $endPoint;

    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
		$this->saveImage = env('APP_UPLOAD_PATH');
		$this->endPoint  = env('APP_URL');
    }

    /**
     * CREATE VOUCHER
     */
    // DB
    function createVoucher($type, $post) {
    	$data = [];

    	switch ($type) {
    		case 'single':
    			$save = Voucher::create($post);

		    	if ($save) {
		    		return true;
		    	}
		    	else {
		    		return false;
		    	}

    			break;
    		
    		default:
    			$voucher = explode("\n", $post['code']);

    			$voucher = array_filter($voucher);

    			if (empty($voucher)) {
    				return false;
    			}
    			else {
    				$data = [];

    				foreach ($voucher as $value) {
    					$temp = [
                            'code'     => trim($value),
                            'status'   => 0,
                            'category' => $post['category']
    					];

    					array_push($data, $temp);
    				}

    				$save = Voucher::insert($data);

    				if ($save) {
    					return true;
    				}
    				else {
    					return false;
    				}
    			}

    			break;
    	}
    }

    // DB GENERATE
    function generateVoucher($total, $category) {
    	$data = [];

    	$codeList = [];

    	for ($i=0; $i < $total ; $i++) { 
    		$code = MyHelper::createrandom(10);

    		while (in_array($code, $codeList)) {
				$code = MyHelper::createrandom(10);    			
    		}
    		
    		$temp = [
                'code'     => $code,
                'status'   => 0,
                'category' => $category
    		];

    		array_push($data, $temp);
    	}

    	$save = Voucher::insert($data);

    	if ($save) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    // CREATE
    function create(Request $request) {
    	$data = array(
    	        'title'   => 'Create Voucher',
    	        'menu'    => 'voucher',
    	        'submenu' => 'create',
    	    );

    	$post = $request->all();

    	if (empty($post)) {
    		return view('voucher.create', $data);
    	}
    	else {
    		// print_r($post); exit();
    		if ($post['type'] == "random") {
    			$create = $this->generateVoucher($post['jumlah'], $post['category']);
    		}
    		else {
    			unset($post['type']);
    			$create = $this->createVoucher('more', $post);
    		}

    		if ($create) {
    			session(['success' => ['s' => 'Voucher has been created.']]);

    			return back();
    		}
    		else {
    			return back()->withErrors(['Something went wrong. Please try again.'])->withInput();
    		}
    	}
    }

    /**
     * =====================================================================================================================
     * =====================================================================================================================
     */
    
    /**
     * UPDATE VOUCHER
     */
    // DB
    function updateVoucher($key, $post) {
    	if (isset($post['code'])) {
    		$data['code'] = $post['code'];
    	}

    	if (isset($post['status'])) {
    		$data['status'] = $post['status'];
    	}

    	$save = Voucher::where('id', $key)->update($data);

    	if ($save) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    /**
     * DELETE VOUCHER
     */
    // Delete
    function delete(Request $request) {
        $post = $request->all();

        if ($this->deleteVoucher($post['id'])) {
            $result = "yes";
        }
        else {
            $result = "no";
        }

        return $result;
    }
    
    // DB
    function deleteVoucher($key) {
    	$delete = Voucher::where('id', $key)->delete();

    	if ($delete) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    // delete voucher free
    function deleteAllVoucherFree(Request $request) {
        $delete = Voucher::where('status', '=', '0')->delete();

        if ($delete) {
            return "yes";
        }
        else {
            return "no";
        }
    }
    
    /**
     * LIST VOUCHER
     */
    // LIST
    function listVoucherBy($type, Request $request) {
    	$data = array(
                'title'      => 'Voucher '.ucfirst($type),
                'menu'       => 'voucher',
                'submenu'    => 'list',
                'subsubmenu' => $type,
    	    );

        $postWeb = $request->all();

    	switch ($type) {
    		case 'free':
    			$post['free'] = 'yes';

    			break;
    		
    		case 'used':
                $post['used'] = 'yes';

                if (!empty($postWeb)) {
                    if (!isset($postWeb['page'])) {
                        $post['start_date'] = $postWeb['start_date'];
                        $post['end_date']   = $postWeb['end_date'];

                        session(['start_date' => $post['start_date']]);
                        session(['end_date'   => $post['end_date']]);
                    }
                    else {
                        $post['start_date'] = session::get('start_date');
                        $post['end_date']   = session::get('end_date');

                        session(['start_date' => $post['start_date']]);
                        session(['end_date'   => $post['end_date']]);
                    }
                }
                else {
                    $post['start_date'] = date('Y-m-d', strtotime('- 1 week'));
                    $post['end_date']   = date('Y-m-d');
                }

    			break;
    		
    		default:
    			return view('errors.404');
    	}

        $data['data'] = $this->listVoucher($post);
        $data['post'] = "yes";
        
    	$data['type'] = $type;

    	// print_r($data); exit();
        
        $info = json_decode(json_encode($data['data']), true);
        $data['total_voucher'] = $info['total'];

        if (isset($postWeb['start_date']) && isset($postWeb['end_date'])) {
            // $request->session()->forget('start_date');
            // $request->session()->forget('end_date');
            
            $data['start_date'] = $post['start_date'];
            $data['end_date']   = $post['end_date'];
        }
        else {

            if (isset($postWeb['page'])) {
                $data['start_date'] = session('start_date');
                $data['end_date']   = session('end_date');
            }
            else {
                $data['start_date'] = date('Y-m-d', strtotime('- 1 week'));
                $data['end_date']   = date('Y-m-d');
            }
        }

        if ($type == "used") {
            for ($x=1; $x <= 4; $x++) {
                $data['v_'.''.$x] = Voucher::where('status', '=', '1')->where('category', '=', "$x")->whereBetween('updated_at', [$data['start_date'].' 00:00:00', $data['end_date'].' 23:59:59'])->count();
            }
        }
        else {
            for ($x=1; $x <= 4; $x++) {
                $data['v_'.''.$x] = Voucher::where('status', '=', '0')->where('category', '=', "$x")->count();
            }
        }

    	return view('voucher.list', $data);
    }

    // DB
    function listVoucher($post) {
    	$data = Voucher::select('*');

    	if (isset($post['used'])) {
    		$data->where('status', '=', '1');
    	}

    	if (isset($post['free'])) {
    		$data->where('status', '=', '0');
    	}

        if (isset($post['start_date']) && isset($post['end_date'])) {
            $start = $post['start_date'].' 00:00:00';
            $end   = $post['end_date'].' 23:59:59';
            
            $data->whereBetween('updated_at', [$start, $end]);
        }

        if (isset($post['category'])) {
            $data->where('category', $post['category']);
        }

    	$data = $data->paginate(10);

		if (isset($post['used'])) {
			foreach ($data as $key => $value) {
				$menu = Win::join('Menu', 'Menu.id', '=', 'Win.id_menu')->select('Menu.id', 'Menu.name')->where('id_voucher', $value->id)->get();

					$data[$key]->menu = $menu;	
			}
		}

    	return $data;
    }

}
