<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Voucher;
use App\Win;
use App\Setting;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public $saveImage;
    public $endPoint;

    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
		$this->saveImage = env('APP_UPLOAD_PATH');
		$this->endPoint  = env('APP_URL');
    }

    public function getSetting(Request $request) {
    	$data = array(
    	        'title'   => 'Setting',
    	        'menu'    => 'setting',
    	        'submenu' => 'list',
    	    );

    	$post = $request->all();

    	if (empty($post)) {
    		$setting = Setting::get()->toArray();

    		if (!empty($setting)) {
    			
                foreach ($setting[0] as $key => $value) {
                    $data[$key] = $value;
                }

    		}

    		return view('setting.list', $data);
    	}
    	else {
    		// print_r($post); exit();
    		
    		unset($post['_token']);

    		$update = DB::table('setting')->update($post);

    		if ($update) {
    			session(['success' => ['s' => 'Setting has been updated.']]);

    			return back();
    		}
    		else {

    			$save = Setting::create($post);

    			if ($save) {
    				session(['success' => ['s' => 'Setting has been updated.']]);

    				return back();
    			}
    			else {
    				return back()->withErrors(['Something went wrong. Please try again.'])->withInput();	
    			}

    		}
    	}  	
    }


}
