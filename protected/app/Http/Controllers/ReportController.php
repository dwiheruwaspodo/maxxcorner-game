<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Voucher;
use App\Win;
use DB;

use App\Lib\MyHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
		$this->saveImage = env('APP_UPLOAD_PATH');
		$this->endPoint  = env('APP_URL');
    }

    function report(Request $request) {
    	$report = $this->reportMenuHarga([]);

    	$data = array(
	        'title'   => 'Report',
	        'menu'    => 'report',
	        'submenu' => 'global',
	    );

    	if (empty($report)) {
			$data['menu_list']  = [];
			$data['total_menu'] = 0;
			$data['money_out']  = 0;
			$data['menu_out']   = 0;
    	}
    	else {
			$data['menu_list']  = $report['menu'];
			$data['money_out']  = $report['money_out'];
			$data['menu_out']   = $report['menu_out'];
			$data['total_menu'] = count($data['menu_list']);
    	}

    	return view('report.menu_pengeluaran', $data);
    }

    function reportMenuHarga($post) {
    	$menuAll = Menu::select(
    			'id',
    			'plu_id',
    			'name',
    			'prices',
    			'picture',
    			DB::raw('if(picture is not null, (select concat("'.$this->endPoint.'", picture)), "'.$this->endPoint.'assets/pages/img/noimg-500-375.png") as url_picture')
    		)->get()->toArray();

    	if (empty($menuAll)) {
    		$result = [];
    	}
    	else {
			$pengeluaran          = 0;
			$totalMenuKeluarSemua = 0;

    		foreach ($menuAll as $key => $value) {
    			/**
    			 * total menu out
    			 */
    			if (isset($post['start_date']) && isset($post['end_date'])) {
			        $start = $post['start_date'].' 00:00:00';
			        $end   = $post['end_date'].' 23:59:59';
			        
			        $totalMenuOut = Win::join('voucher', 'win.id_voucher', '=', 'voucher.id')->where('id_menu', $value['id'])->whereBetween('updated_at', [$start, $end])->count();
			    }
			    else {
					$totalMenuOut = Win::where('id_menu', $value['id'])->count();
			    }

				$menuAll[$key]['total_out'] = $totalMenuOut;

				/**
				 * total
				 */
				$menuAll[$key]['total_prices'] = $value['prices'] * $totalMenuOut;

				/**
				 * pengeluaran
				 */
				$pengeluaran = $pengeluaran + $menuAll[$key]['total_prices'];

				/**
				 * Total pengeluaran menu semua
				 */
				$totalMenuKeluarSemua = $totalMenuKeluarSemua + $totalMenuOut;
    		}

    		$result = [
				'menu'      => $menuAll,
				'money_out' => $pengeluaran,
				'menu_out'  => $totalMenuKeluarSemua
    		];
    	}

    	return $result;
    }

    function reportByDate(Request $request) {
    	$data = array(
	        'title'   => 'Report',
	        'menu'    => 'report',
	        'submenu' => 'range',
	    );

    	$post = $request->all();
    	if ($post) {
    	    $post['start_date'] = $post['start_date'];
    	    $post['end_date']   = $post['end_date'];
    	}
    	else {
    	    $post['start_date'] = date('Y-m-d', strtotime('- 1 week'));
    	    $post['end_date']   = date('Y-m-d');
    	}

    	$report = $this->reportMenuHarga($post);

    	if (empty($report)) {
			$data['menu_list']  = [];
			$data['total_menu'] = 0;
			$data['money_out']  = 0;
			$data['menu_out']   = 0;
    	}
    	else {
			$data['menu_list']  = $report['menu'];
			$data['money_out']  = $report['money_out'];
			$data['menu_out']   = $report['menu_out'];
			$data['total_menu'] = count($data['menu_list']);
    	}

		$data['start_date'] = $post['start_date'];
		$data['end_date']   = $post['end_date'];

		return view('report.menu_pengeluaran_by_date', $data);
    }

}
