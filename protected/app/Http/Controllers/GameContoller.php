<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Menu;
use App\Voucher;
use App\Win;
use App\Setting;
use DB;

use App\Lib\MyHelper;

use App\Http\Controllers\VoucherController;
use App\Http\Controllers\MenuController;

use File;

class GameContoller extends Controller
{
    public $menu;
    public $invoiceUp;

    public function __construct()
    {
        // $this->middleware('auth');
		date_default_timezone_set('Asia/Jakarta');
		$this->menu      = new MenuController;
		$this->voucher   = new VoucherController;
		$this->invoiceUp = env('APP_UPLOAD_TEXT');
    }

    function randomImage(Request $request) {

    	// get setting
    	$setting = $this->getSetting();

    	if (empty($setting)) {
    		$result = [
    			'status' => 'fail',
    			'messages' => ['please setting preferences']
    		];
    	}
    	else {
    		// peluang
	    	// $chance = [1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
	    	// $chance = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4];
	    	$chance = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4];
	    	
	    	// shuffle array
	    	shuffle($chance);

	    	// random
	    	$random = array_rand($chance);

	    	$cek = Voucher::where('status', '=', '0')->where('category', '=', "$chance[$random]")->get()->toArray();
	    	
	    	$tampung = array();

	    	while (count($cek) == 0) {
	    		$random = array_rand($chance);
	    		if(!in_array($random, $tampung)) {
	    			array_push($tampung, $random);
	    			$cek = Voucher::where('status', '=', '0')->where('category', '=', "$chance[$random]")->get()->toArray();
	    		} else {
	    			return array('status' => 'fail', 'messages' => ['stock empty']);
	    		}
	    	}

	    	// get
	    	$totalView = $chance[$random];

	    	$menu = [];

	    	for ($x=0; $x < 4; $x++) {
	    		if ($x < $totalView) {
			    	$searchMenu = $this->menu->listMainMenu([]);

			    	if (!empty($searchMenu)) {
						$key      = array_rand($searchMenu);
						$menu[$x] = $searchMenu[$key];
			    	}
			    	else {
			    		$menu[$x] = null;
			    	}
	    		}
	    		else {
	    			$menu[$x] = null;
	    		}
	    	}

	    	// cek hasil menu kosong atau ada isi
	    	$cek = array_filter($menu);

	    	if (!empty($cek)) {
	    		shuffle($menu);

	    		$isi = array_filter($menu);

				$total = 0;
				$id    = "";
	    		foreach ($isi as $key => $value) {
					$total = $total + $value['prices'];
					$id    .= $value['id'].',';
	    		}

	    		$id = substr($id, 0, -1);

	    		/**
	    		 * deviasi
	    		 */
	    		$voucherType = count($cek);

	    		$result = [
					'status' => 'success',
					'data'   => [
						'menu'          => $menu,
						'total_prices'  => $total,
						'deviasi_atas'  => $total + $setting[0]['max_'.$voucherType],
						'deviasi_bawah' => $total - $setting[0]['min_'.$voucherType],
						'id_menu'		=> $id,
						'time'			=> (int) $setting[0]['time']
	    			]
	    		];
	    	}
	    	else {
	    		$result = [
					'status'   => 'fail',
					'messages' => ['stock is empty']
	    		];
	    	}
    	}
    	
    	return $result;
    }

    function getSetting(){
    	$setting = Setting::get()->toArray();

    	return $setting;
    }

    function youWin(Request $request) {
		$post    = $request->json()->all();
		$id_menu = explode(",", $post['id_menu']);

		$category = count($id_menu);
		
		// get voucher yang kosong
		$voucher = Voucher::where('status', '=', "0")->where('category', '=' , "$category")->get()->toArray();

		// return $voucher;

		if (empty($voucher)) {
			$result = [
				'status' => 'fail',
				'messages' => ['voucher is empty']
			];
		}
		else {
			// print_r($voucher);
			$voucher = $voucher[0];
			// array untuk menampung voucher yg menang di tabel win
			$voucherWin = [];

			// text
			$text = [];
			
			foreach ($id_menu as $value) {
				// update menu
				$menu = $this->menu->listMainMenu(['id' => $value]);

				if (empty($menu)) {
					$result = [
						'status' => 'fail',
						'messages' => ['empty menu']
					];

					return $result;
				}
				// get to data file
				array_push($text, $menu[0]);

				// menu for voucher
				$dataVoucher = [
					'id_voucher' => $voucher['id'],
					'id_menu'	=> $value
				];

				array_push($voucherWin, $dataVoucher);
				
			}

			// print_r($text); exit();

			$winner = Win::insert($voucherWin);

			if ($winner) {
				// update status voucher
				$updateVoucher = $this->voucher->updateVoucher($voucher['id'], ['status' => 1]);

				if ($updateVoucher) {

					// get invoice
					$inv = $this->createInvoice($voucher, $text);
					// print_r($_SERVER["DOCUMENT_ROOT"].'/'.$inv);
					$filepath = $_SERVER["DOCUMENT_ROOT"] . '/' . $inv;

					// $filepath = preg_replace('/C:\/xampp\/htdocs\/tebakharga\//', 'C:\\\\xampp\\\\htdocs\\\\tebakharga\\\\', $filepath);

					// print order here
					// change the computer name and printer name
					$exec = exec('print /d:\\\\MKT-P-DELLA\bixolon "' . $filepath . '"');
					// $exec = exec('notepad -p ' . $filepath);

					$result = [
						'status' => 'success',
						'result' => [
							'exec' => $exec,
							'path' => $filepath
						]
					];
				}
				else {
					$result = [
						'status' => 'fail',
						'messages' => ['fail create invoice']
					];
				}
			}
			else {
				$result = [
						'status' => 'fail',
						'messages' => ['fail save to winner']
					];
			}
		}

		return $result;
    }

    function createInvoice($voucher, $text) {
    	$length = 40;
    	$textInvoice = "";

    	$setting = Setting::get()->first();

    	// write to txt : MAXX CORNER
    	$title = 'MAXX CORNER';
    	$title_length = strlen($title);
    	$title_space_left = floor(($length - $title_length) / 2);
    	$textInvoice .= $this->space($title_space_left) . $title . "\n";

    	// write to txt : date and order 
    	$today_text = date('dmY');
    	$voucher_total_db = Voucher::whereDate('updated_at', DB::raw('CURDATE()'))->count();
    	$voucher_order = $voucher_total_db;
    	if($voucher_total_db < 10) {
    		$voucher_order = "0".$voucher_total_db;
    	}
    	$order = $today_text . "#" . $voucher_order;
    	$order_length = strlen($order);
    	$order_left = floor(($length - $order_length) / 2);
    	$textInvoice .= $this->space($order_left) . $order;

    	$textInvoice .= "\n\n";

    	// write to txt : Selamat Anda berhak mendapatkan
    	$textInvoice .= "Selamat Anda berhak mendapatkan: \n\n";
    	// write to txt : the items
    	foreach ($text as $key => $value) {
			$textInvoice .= "- ".$value['plu_id'].' - '.$value['name']."\n" ;
		}
		$textInvoice .= "\n";

		// write to txt : Kode Voucher Anda:
		$text_kode_voucher = "Kode Voucher Anda:\n";
		$text_kode_length = strlen($text_kode_voucher);
		$text_kode_left = floor(($length - $text_kode_length) / 2);
		$textInvoice .= $this->space($text_kode_left) . $text_kode_voucher;

		// write to txt : voucher_code
		$kode_voucher = trim($voucher['code']);
		$voucher_length = strlen( $kode_voucher );
		$voucher_space_left = floor(($length - $voucher_length) / 2);
		$textInvoice .= $this->space($voucher_space_left) . $kode_voucher;

		$textInvoice .= "\n\n";

		// write to txt : Gunakan Sebelum:
		$gunakan = "Gunakan Sebelum:";
		$gunakan_length = strlen($gunakan);
		$gunakan_left = floor(($length - $gunakan_length) / 2);
		$textInvoice .= $this->space($gunakan_left) . $gunakan;
		$textInvoice .= "\n";

		// write to txt : expired date
		$expired = date('d F Y', strtotime($setting->voucher_expired));
		$expired_length = strlen($expired);
		$expired_left = floor(($length - $expired_length) / 2);
		$textInvoice .= $this->space($expired_left) . $expired;

		$textInvoice .= "\n\n\n";

		// write to txt : _0_
		$end_text = "_0_";
		$end_text_length = strlen($end_text);
		$end_text_left = floor(($length - $end_text_length) / 2);
		$textInvoice .= $this->space($end_text_left) . $end_text . "\n";

		//write to txt : dash(------)
		$textInvoice .= $this->dash($length);
		$textInvoice .= "\n\n";
    	
		$fileName = $this->invoiceUp.trim($voucher['code']).'_'.time().'.txt';
    	$save = File::put($fileName, $textInvoice);
    	return $fileName;
    }

    private function space($length) {
    	$space = ""; 
    	for($x = 1; $x <= $length; $x++ ) {
    		$space .= " ";
    	}
    	return $space;
    }

    private function dash($length) {
    	$dash = ""; 
    	for($x = 1; $x <= $length; $x++ ) {
    		$dash .= "-";
    	}
    	return $dash;
    }
    
}
