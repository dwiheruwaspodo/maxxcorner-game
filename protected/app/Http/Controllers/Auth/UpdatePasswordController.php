<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller{
	public function __construct() {
		$this->middleware('auth');
	}

	public function view(Request $request) {
		$data = array(
				'menu' => '',
				'submenu' => '',
			);
		return view('auth.change_password', $data);
	}

	public function update( Request $request) {
		$this->validate($request, [
				'old' => 'required',
				'password' => 'required|confirmed',
				'password_confirmation' => 'required',
			]);

		$user = User::find(Auth::id());
		$hashedPassword = $user->password;

		if(Hash::check($request->old, $hashedPassword)) {
			$user->fill([
					'password' => Hash::make($request->password)
				])->save();
			$request->session()->flash('success', 'Your password has been changed');
			return back();
		}

		$request->session()->flash('failure', 'Your old password does\'t match');
		return back();
	}
}
