<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
        public function __construct() {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        $data = array(
                'title'   => 'Dashboard',
                'menu'    => 'dashboard',
                'submenu' => '',
            );

    	return view('dashboard', $data);
    }
}
