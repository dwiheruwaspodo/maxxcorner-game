<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $code
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property Menu[] $menus
 */
class Voucher extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'voucher';

    /**
     * @var array
     */
    protected $fillable = ['code', 'status', 'created_at', 'updated_at', 'category'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany('App\Http\Models\Menu', 'win', 'id_voucher', 'id_menu');
    }
}
