<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $max
 * @property integer $min
 */
class Setting extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'setting';

    /**
     * @var array
     */
    protected $fillable = [
        'max_1', 
        'max_2',
        'max_3',
        'max_4',
        'min_1', 
        'min_2',
        'min_3',
        'min_4', 
        'time', 
        'voucher_expired'
    ];

    public $timestamps = false;
}
