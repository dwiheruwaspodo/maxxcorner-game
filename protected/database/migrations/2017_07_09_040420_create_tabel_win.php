<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelWin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('win', function(Blueprint $table) {
            $table->unsignedInteger('id_voucher');
            $table->foreign('id_voucher', 'id_voucher_win')->references('id')->on('voucher');

            $table->unsignedInteger('id_menu');
            $table->foreign('id_menu', 'id_menu_win')->references('id')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('win');
    }
}
