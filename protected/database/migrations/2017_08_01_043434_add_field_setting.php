<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddFieldSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `setting` CHANGE `min` `min_1` INT(11) NULL;");
        DB::statement("ALTER TABLE `setting` CHANGE `max` `max_1` INT(11) NULL;");

        Schema::table('setting', function(Blueprint $table) {
            $table->integer('max_2')->nullable()->after('max_1');
            $table->integer('max_3')->nullable()->after('max_2');
            $table->integer('max_4')->nullable()->after('max_3');

            $table->integer('min_2')->nullable()->after('min_1');
            $table->integer('min_3')->nullable()->after('min_2');
            $table->integer('min_4')->nullable()->after('min_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `setting` CHANGE `min_1` `min` INT(11) NOT NULL;");
        DB::statement("ALTER TABLE `setting` CHANGE `max_1` `max` INT(11) NOT NULL;");

        Schema::table('setting', function(Blueprint $table) {
            $table->dropColumn('max_2');
            $table->dropColumn('max_3');
            $table->dropColumn('max_4');

            $table->dropColumn('min_2');
            $table->dropColumn('min_3');
            $table->dropColumn('min_4');
        });
    }
}
