<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
				'name'           => 'Admin',
				'email'          => 'admin@example.com',
				'password'       => bcrypt('iloveindonesia'),
				'remember_token' => 'QzTuCQVe6e7zhUPyIpANfTJ48TiaMcSwjFQfKQp5EMnpweaV9Fcdyi6da1cT',
				'created_at'     => Carbon::now(),
				'updated_at'     => Carbon::now()
        	]);
    }
}
