$(document).ready(function() {
		// setTimeout(function(){
		// 	createjs.Sound.play("roll");	
		// },1000);
		var delay = [300, 325, 350, 375, 400, 424, 450, 475, 500];

		function randomize( id ) {
			var random = Math.floor(Math.random() * $(id).find('li').length);
			$(id).find('li').hide();
			$( id + ' > li:eq(' + random + ')').fadeIn();
		}

		function fireworks() {
			var r = 4 + parseInt(Math.random() * 20);
			for (var i = r; i--;) {
			    setTimeout('createFirework(50,200,8,5,null,null,null,null,true,true)', (i + 1) * (1 + parseInt(Math.random() * 1000)));
			}
			return false;
		}
		
		$('.cover').hide();

		var funct_random_0 = setInterval(randomize, delay[Math.floor(Math.random() * delay.length)], '#flip_0');
		var funct_random_1 = setInterval(randomize, delay[Math.floor(Math.random() * delay.length)], '#flip_1');
		var funct_random_2 = setInterval(randomize, delay[Math.floor(Math.random() * delay.length)], '#flip_2');
		var funct_random_3 = setInterval(randomize, delay[Math.floor(Math.random() * delay.length)], '#flip_3');

		$.ajax({
			url: url  + '/api/random',
			data: '',
			method: 'get',
			success: function(result) {
				// console.log(result);
				if(result.status != 'fail'){
					var data = result.data;
					var timer = data.time;
					var menus   = data.menu;
					var id_menu = data.id_menu;

					// don't forget to disable this log below
					console.log(data);
					setTimeout(function() {
						clearInterval(funct_random_0);
						if(menus[0] != null) {
							$('#box_0').html('<img src="' + menus[0].url_picture + '" alt="" class="img-responsive cover"><div class="text-name"><center><p class="color-black">' + menus[0].name + '</p></center></div>');
						}else {
							$('#box_0').html('<img src="image/default.png" alt="default" class="img-responsive cover"><div class="text-name"><center><p class="color-black">None</p></center></div>');
						}
					}, 4000);

					setTimeout(function() {
						clearInterval(funct_random_1);
						if(menus[1] != null) {
							$('#box_1').html('<img src="' + menus[1].url_picture + '" alt="" class="img-responsive cover"><div class="text-name"><center><p class="color-black">' + menus[1].name + '</p></center></div>');
						}else {
							$('#box_1').html('<img src="image/default.png" alt="default" class="img-responsive cover"><div class="text-name"><center><p class="color-black">None</p></center></div>');
						}
					}, 5000);

					setTimeout(function() {
						clearInterval(funct_random_2);
						if(menus[2] != null) {
							$('#box_2').html('<img src="' + menus[2].url_picture + '" alt="" class="img-responsive cover"><div class="text-name"><center><p class="color-black">' + menus[2].name + '</p></center></div>');
						}else {
							$('#box_2').html('<img src="image/default.png" alt="default" class="img-responsive cover"><div class="text-name"><center><p class="color-black">None</p></center></div>');
						}
					}, 6000);

					setTimeout(function() {
						clearInterval(funct_random_3);
						createjs.Sound.play("bell");

						if(result.status == 'success') {
							var total   = parseInt(data.total_prices);
							var top     = parseInt(data.deviasi_atas);
							var bottom  = parseInt(data.deviasi_bawah);
							// console.log(menus);

							if(menus[3] != null) {
								$('#box_3').html('<img src="' + menus[3].url_picture + '" alt="" class="img-responsive cover"><div class="text-name"><center><p class="color-black">' + menus[3].name + '</p></center></div>');
							}else {
								$('#box_3').html('<img src="image/default.png" alt="default" class="img-responsive cover"><div class="text-name"><center><p class="color-black">None</p></center></div>');
							}

							$('#form-tebak').show();
							var intervalTimeout = setInterval(function(){
								if(timer >= 0) {
									if(timer < 4) {
										$('#timer').html('<h1 style="color:red" class="animated flash big">' + timer + '</h1>');
									}else if(timer < 7 ) {
										$('#timer').html('<h1 style="color:orange" class="big">' + timer + '</h1>');
									} else {
										$('#timer').html('<h1 style="color:#222" class="big">' + timer + '</h1>');
									}
									
									timer--
								}else {
									$('.overlay_timeout').removeClass( "zoomIn hide" );
									$('.overlay_timeout').addClass( "animated zoomIn show" );
									createjs.Sound.play("timeout");
									$('.overlay_timeout').find('#back').focus();
									clearInterval(intervalTimeout);
								}
							},1000);

							
						} else {
							swal({
								title : 'Failed to fetch data. Please contact the manager.',
								type : 'warning'
							});
						}

						$('#tebak-harga').submit(function(e){
							e.preventDefault();
							clearInterval(intervalTimeout);

							var tebakan = $('#tebakan').val();
							
							if(tebakan == total) {
								$('.overlay_win').removeClass( "zoomIn hide" );
								$('.overlay_win').addClass( "animated zoomIn show" );
								createjs.Sound.play("congratulation");
								fireworks();
								$('.overlay_win').find('#back').focus();
								$.ajax({
									url:  url + '/api/winner',
									data : JSON.stringify({ id_menu : id_menu }),
									contentType: "application/json",
									type : 'post',
									success : function(data) {
										console.log(data);
									},
									error : function(error) {
										console.log(error);
									}
								});
							} else if( tebakan > bottom && tebakan < top) {
								$('.overlay_win').removeClass( "zoomIn hide" );
								$('.overlay_win').addClass( "animated zoomIn show" );
								createjs.Sound.play("congratulation");
								fireworks();
								$('.overlay_win').find('#back').focus();

								$.ajax({
									url:  url + '/api/winner',
									data : JSON.stringify({ id_menu : id_menu }),
									contentType: "application/json",
									type : 'post',
									success : function(data) {
										console.log(data);
									},
									error : function(error) {
										console.log(error);
									}
								});
							}else {
								$('.overlay_lost').removeClass( "zoomIn hide" );
								$('.overlay_lost').addClass( "animated zoomIn show" );
								createjs.Sound.play("lost");
								$('.overlay_lost').find('#back').focus();
							}
						});
					}, 7000);
				} else {
					swal({
						title : 'Failed to fetch data. Please contact the manager.',
						type : 'warning'
					});
				}
			},
			error: function(data) {
				console.log('error');
			},
			beforeSend: function() {
				setTimeout(function(){
					createjs.Sound.play("roll");	
				},1000);
			}
		});
		
	});